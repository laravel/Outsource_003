﻿$(function() {
    $('header .back').click(function(event) {
        history.go(-1);
        return false;
    });
});

var layer = (function($, undefined){
    var _html = '<div class="layer" style="position: fixed;  left: 0px; top: 0px; z-index: 1018; width: 100%; height: 100%; text-align: center; cursor: pointer; display: block; opacity: 1;"><div style="display:inline-block;height:100%;vertical-align:middle;"></div><div class="closer" style="position:absolute;left:0;top:0;width:100%;height:100%;background:#000;opacity:0.7;filter:alpha(opacity=70);"></div><div class="layer_box" style="color: rgb(0, 0, 0); display: inline-block; vertical-align: middle; position: relative; z-index: 1; cursor: pointer; width: 100%; min-height: 100px;  height: auto;"></div></div>'; //最外层边框
    var jQ_html = $(_html);

   function closer(){
       $('.layer').remove();
   };

   function bind(){

   };

   return function( content ){
       var _content = content;
       jQ_html.find('.layer_box').html(_content);
       $('.layer').remove();
       $('body').append(jQ_html)
       $('.layer .closer, .layer .close').on('click', closer);
   };
})($);
$(function(){
    $(".order-box .order-header ").on("click", function(){
        var box = $(this).parents(".order-box");
        if(!box.is('.open')){
            box.addClass("open");
            return;
        }
        box.removeClass("open");
    });
});
